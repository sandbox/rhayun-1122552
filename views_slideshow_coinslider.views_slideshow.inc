<?php
// $Id$

/**
 *  @file
 *  The default options available with Views Slideshow: Coin Slider.
 */

/**
 *  Implements hook_views_slideshow_modes().
 */
function views_slideshow_coinslider_views_slideshow_modes() {
  // Expose the mode for coinslider slide shows.
  $options = array(
    'views_slideshow_coinslider' => t('Coin Slider'),
  );
  return $options;
}

/**
 *  Implements hook_views_slideshow_option_definition().
 */
function views_slideshow_coinslider_views_slideshow_option_definition() {
  // Set our default options.
  $options = array(
    'views_slideshow_coinslider' => array(
      'default' => array(
  			'width' => 565,
				'height' => 290,
				'spw' => 7,
				'sph' => 5,
				'delay' => '3000',
				'sDelay' => '30',
				'opacity' => '0.7',
				'titleSpeed' => '500',
				'effect' => '',
				'navigation' => true,
				'links' => true,
				'hoverPause' => true,
      ),
    ),
  );
  return $options;
}

/**
 *  Implements hook_views_slideshow_options_form().
 */
function views_slideshow_coinslider_views_slideshow_options_form(&$form, &$form_state, &$view) {
  // Create the form elements for our Coin Slider view options.
  $form['views_slideshow_coinslider'] = array(
    '#type' => 'fieldset',
    '#title' => t('Coin Slider options'),
    '#collapsible' => TRUE,
    '#collapsed' => !($view->options['mode'] == 'views_slideshow_coinslider'),
  );
  
  $form['views_slideshow_coinslider']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('Width of slider panel.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['width'],
  );
  
  $form['views_slideshow_coinslider']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Height of slider panel'),
    '#default_value' => $view->options['views_slideshow_coinslider']['height'],
  );
  
  $form['views_slideshow_coinslider']['spw'] = array(
    '#type' => 'textfield',
    '#title' => t('SPW'),
    '#description' => t('Squares per width'),
    '#default_value' => $view->options['views_slideshow_coinslider']['spw'],
  );
  
  $form['views_slideshow_coinslider']['sph'] = array(
    '#type' => 'textfield',
    '#title' => t('SPH'),
    '#description' => t('Squares per height'),
    '#default_value' => $view->options['views_slideshow_coinslider']['sph'],
  );
  
  $form['views_slideshow_coinslider']['delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay'),
    '#description' => t('Delay between images in ms'),
    '#default_value' => $view->options['views_slideshow_coinslider']['delay'],
  );
  
  $form['views_slideshow_coinslider']['sDelay'] = array(
    '#type' => 'textfield',
    '#title' => t('sDelay'),
    '#description' => t('Delay beetwen squares in ms.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['sDelay'],
  );
  
  $form['views_slideshow_coinslider']['opacity'] = array(
    '#type' => 'select',
    '#title' => t('Opacity'),
    '#description' => t('Opacity of title and navigation.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['opacity'],
    '#options' => array(
      '0.1' => '0.1',
		  '0.2' => '0.2',
		  '0.3' => '0.3',
		  '0.4' => '0.4',
		  '0.5' => '0.5',
		  '0.6' => '0.6',
		  '0.7' => '0.7',
		  '0.8' => '0.8',
		  '0.9' => '0.9',
		  '1' => '1',
    ),
  );
  
  $form['views_slideshow_coinslider']['titleSpeed'] = array(
    '#type' => 'textfield',
    '#title' => t('titleSpeed'),
    '#description' => t('Speed of title appereance in ms.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['titleSpeed'],
  );
  
  $form['views_slideshow_coinslider']['effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect'),
    '#description' => t('random, swirl, rain, straight.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['effect'],
    '#options' => array(
      'random' => t('random'),
      'swirl' => t('swirl'),
      'rain' => t('rain'),
  		'straight' => t('straight'),
    ),
  );
  
  $form['views_slideshow_coinslider']['navigation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigation'),
    '#description' => t('Prev next and buttons.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['navigation'],
  );
  
  $form['views_slideshow_coinslider']['links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Links'),
    '#description' => t('How images as links.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['links'],
  );
  
  $form['views_slideshow_coinslider']['hoverPause'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hover Pause'),
    '#description' => t('Pause on hover.'),
    '#default_value' => $view->options['views_slideshow_coinslider']['hoverPause'],
  );
  
}
