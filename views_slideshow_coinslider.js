// $Id$

/**
 *  @file
 *  This will initiate any Coin Slider browsers we have set up.
 */
Drupal.behaviors.viewsSlideshowCoinSlider = function (context) {
  $('.views-slideshow-coinslider-images:not(.viewsSlideshowCoinSlider-processed)', context).addClass('viewsSlideshowCoinSlider-processed').each(function () {
	var id = $(this).attr('id');
	var coin = Drupal.settings.CoinSlider[id];

	$(this).coinslider({
		CoinSliderID: id,
		width: coin['width'], // width of slider panel
		height: coin['height'], // height of slider panel
		spw: coin['sqw'] ? coin['spw'] : 7, // squares per width
		sph: coin['sph'] ? coin['sph'] : 5, // squares per height
		delay: coin['delay'] ? coin['delay'] : 3000, // delay between images in ms
		sDelay: coin['sdelay'] ? coin['sdelay'] : 30, // delay beetwen squares in ms
		opacity: coin['opacity'] ? coin['opacity'] : 0.7, // opacity of title and navigation
		titleSpeed: coin['titlespeed'] ? coin['titlespeed'] : 500, // speed of title appereance in ms
		effect: coin['effect'] ? coin['effect'] : '', // random, swirl, rain, straight
		navigation: coin['navigation'], // prev next and buttons
		links : coin['links'], // show images as links
		hoverPause: coin['hoverPause'] // pause on hover
	});
	  
  });
};
