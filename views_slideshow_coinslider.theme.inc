<?php
// $Id$

/**
 *  @file
 *  Theme & preprocess functions for the Views Slideshow: Coin Slider module.
 */

/**
 *  We'll grab only the first image from each row.
 */
function template_preprocess_views_slideshow_coinslider(&$vars) {
  // Initialize our $images array.
  $vars['images'] = array();

  // Strip all images from the $rows created by the original view query.
  foreach($vars['rows'] as $item) {
    preg_match('@(<\s*img\s+[^>]*>)@i', $item, $matches);
    if ($image = $matches[1]) {
      // We need to add a URL to 'longdesc', as required by the plugin.
      // If our image is in an anchor tag, use its URL.
      preg_match('@<\s*a\s+href\s*=\s*"\s*([^"]+)\s*"[^>]*>[^<]*'. preg_quote($image) .'[^<]*<\s*/a\s*>@i', $item, $urls);
      if (!($url = $urls[1])) {
        // Otherwise link to the original image.
        preg_match('@src\s*=\s*"([^"]+)"@i', $image, $urls);
        if (!($url = $urls[1])) {
          // If we get this far, there are probably more serious problems.
          // But for now, we'll go to the front page instead.
          $url = url('<front>');
        }
      }

      // Add the URL to the image's longdesc tag.
      $image = preg_replace('@img\s+@i', 'img longdesc="'. $url .'" ', $image);

      // Add the image to our image array to display.
      $vars['images'][] = $image;
    }
  }

  // Find the path to our plugin.
  $path = variable_get('views_slideshow_coinslider_plugin', 'sites/all/libraries/coin-slider');

  // Add the required JS and CSS.
  drupal_add_js($path .'/coin-slider.min.js');
  drupal_add_css($path .'/coin-slider-styles.css');
  drupal_add_js(drupal_get_path('module', 'views_slideshow_coinslider') .'/views_slideshow_coinslider.js');

  $view = $vars['view'];
  $rows = $vars['rows'];
  $options = $vars['options'];

  $settings = array(
    'width' => (int)$options['views_slideshow_coinslider']['width'],
    'height' => (int)$options['views_slideshow_coinslider']['height'],
    'spw' => (int)$options['views_slideshow_coinslider']['spw'],
    'sph' => (int)$options['views_slideshow_coinslider']['sph'],
    'delay' => (int)$options['views_slideshow_coinslider']['delay'],
    'sDelay' => (int)$options['views_slideshow_coinslider']['sDelay'],
    'opacity' => (float)$options['views_slideshow_coinslider']['opacity'],
    'titleSpeed' => (int)$options['views_slideshow_coinslider']['titleSpeed'],
    'effect' => (string)$options['views_slideshow_coinslider']['effect'],
    'navigation' => (bool)$options['views_slideshow_coinslider']['navigation'],
  	'links' => (bool)$options['views_slideshow_coinslider']['links'],
  	'hoverPause' => (bool)$options['views_slideshow_coinslider']['hoverPause'],
  );
  drupal_add_js(array('CoinSlider' => array('views-slideshow-coinslider-images-'. $vars['id'] => $settings)), 'setting');
}
