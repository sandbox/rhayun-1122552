<?php
// $Id$

/**
 * @file
 * Admin functions for the Views Slideshow: Coin Slider module.
 */

/**
 * Configure Views Slideshow: Coin Slider; menu callback for admin/build/views/views_slideshow_coinslider.
 */
function views_slideshow_coinslider_settings() {
  $form = array();

  $path = variable_get('views_slideshow_coinslider_plugin', 'sites/all/libraries/coin-slider');
  $message = t('To use !coinslider, you must first !download the plugin package, extract it, and place the entire contents into the folder on your server specified here.', array('!coinslider' => l(t('Coin Slider'), 'http://workshop.rs/projects/coin-slider'), '!download' => l(t('download'), 'https://code.google.com/p/coin-slider/downloads/list')));
  if (!file_exists($path .'/coin-slider.min.js')) {
    drupal_set_message($message, 'error');
  }

  $form['views_slideshow_coinslider_plugin'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the Coin Slider plugin'),
    '#description' => $message,
    '#default_value' => $path,
  );

  return system_settings_form($form);
}
