// $Id:$

============================
 Views Slideshow: CoinSlider
============================

This module will display a view of images using the Coinslider JavaScript plugin
available from http://workshop.rs/projects/coin-slider.

The module is based on the COIN-SLIDER "Jquery Image Slider with Unique 
Effects" script described in the Ivan Lazarevic 
(http://workshop.rs/projects/coin-slider/) and implements a slider image 
gallery with several slide transition graphical effects.

==============
 Installation
==============

1. Download and install the Views Slideshow module.
2. Extract the contents of the project into your modules directory, probably at /sites/all/modules/views_slideshow_imageflow.
3. Download the Coin Slider Javascript plugin from https://code.google.com/p/coin-slider/downloads/list.
4. Extract the contents of that archive into /sites/all/libraries/coin-slider. You may optionally install that to another folder, but will need to then specify the new location at /admin/build/views/views_slideshow_coinslider.
5. Create a new View with images, using 'Slideshow' for the 'Style', and 'Coin Slider' for the 'Slideshow mode' when configuring the style.


=======
 Notes
=======

It's important to note that if you have anything besides images displayed in
your view, it will be filtered out of the display. This plugin will only
display images. However, it will respect any links the images point to, as
specified by your View. Other than that, it should work with any image fields
in your view, whether from the ImageField, Image, Embedded Media Field (using
thumbnails), or other similar modules.
